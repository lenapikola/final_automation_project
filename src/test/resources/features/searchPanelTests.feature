Feature: check search panel functionality on main page

  @test
  Scenario: test#1 type a city name into location input and press search button
    Given open airbnb.com
    When type text "Berlin" into locationInput
    And press button searchBtn
    Then verify if title has text "Stays in Berlin"
    Then verify if littleSearchPanel exists
    Then verify if littleLocationSearchBtn has text "Berlin"

  @test
  Scenario: test#2 type a non-existent name into location input and press search button
    Given open airbnb.com
    When type text "Qerlin" into locationInput
    And press button searchBtn
    Then verify if travel page title has text "Travel the world with Airbnb"

  @test
  Scenario: test#3 select search by nearby location from location input and press search btn
    Given open airbnb.com
    When press button searchBtn
    And verify if exploreNearbyBtn exists
    And press button exploreNearbyBtn
    And press button searchBtn
    Then verify if title has text "Places to stay near you"

  @test
  Scenario: test#4 enter location, dates and guests data and press search btn
    Given open airbnb.com
    When type text "Lima" into locationInput
    And press button datesInput
    And verify if datesPanel exists
    And select checkIn date
    And select checkOut date
    And press button guestsBtn
    And verify if guestsPanel exists
    And press button adultsIncreaseBtn
    And press button adultsIncreaseBtn
    And verify if adultsQuantityValue has text "2"
    And press button searchBtn
    Then verify if title has text "Stays in"
    Then verify if title has text "Lima"
    Then verify if littleLocationSearchBtn has text "Lima"
    Then verify if littleGuestsBtn has value "2"

  @test
  Scenario: test#5 select 1 adult and 1 infant and click on search button
    Given open airbnb.com
    When type text "Athens" into locationInput
    And press button guestsBtn
    And press button adultsIncreaseBtn
    And verify if adultsQuantityValue has text "1"
    And press button infantsIncreaseBtn
    And verify if infantsQuantityValue has text "1"
    And press button searchBtn
    Then verify if littleGuestsBtn has value "1"

  @test
  Scenario: test#6 select children and infants without selecting adults
    Given open airbnb.com
    When press button guestsBtn
    And verify if adultsQuantityValue has text "0"
    And press button infantsIncreaseBtn
    And verify if infantsQuantityValue has text "1"
    And verify if adultsQuantityValue has text "1"
    And press button adultsDecreaseBtn
    And verify if infantsQuantityValue has text "1"
    And verify if adultsQuantityValue has text "1"
    And press button infantsDecreaseBtn
    And verify if infantsQuantityValue has text "0"
    And verify if adultsQuantityValue has text "1"
    And press button adultsDecreaseBtn
    And verify if adultsQuantityValue has text "0"
    And press button childrenIncreaseBtn
    And press button childrenIncreaseBtn
    And verify if childrenQuantityValue has text "2"
    And verify if adultsQuantityValue has text "1"
    And press button adultsDecreaseBtn
    Then verify if childrenQuantityValue has text "2"
    Then verify if adultsQuantityValue has text "1"

  @test
  Scenario: test#7 scroll main page down and click on search panel little button
    Given open airbnb.com
    When scroll page to view footer
    And verify if littleSearchPanel exists
    And press button littleSearchBtn
    Then verify if datesPanel exists
    Then verify if guestsPanel exists
    Then verify if locationInput exists
    Then verify if searchBtn exists


