Feature: check login functionality on main page

  @test
  Scenario: test#1 login with email
    Given open airbnb.com
    When press button profileBtn
    And verify if profileMenu exists
    And press button loginBtn
    And verify if popupModal exists
    And press button loginWithEmailBtn
    And enter email "jilado2932@95ta.com" and password "cucumber00"
    Then press button submitLoginBtn

