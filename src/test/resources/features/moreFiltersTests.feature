Feature: check more filters functionality on stays page

  @test
  Scenario: test#1 go to moreFilters modal and select different options
    Given open airbnb.com
    When type text "Mexico" into locationInput
    And press button searchBtn
    And press button moreFiltersBtn
    And verify if popupModal exists
    And press button bedsIncreaseBtn
    And verify if bedsQuantityValue has text "1"
    And check checkbox petsAllowed
    And verify if element poolCheckbox has type "checkbox"
    And check checkbox poolCheckbox
    And press button superhostBtn
    And press button moreFiltersSubmitBtn
    Then verify if staysItemList exists
    Then verify if moreFiltersBtn has text "4"

  @test
  Scenario: test#2 go to moreFilters modal and select different options then press clear all filters btn
    Given open airbnb.com
    When type text "Barcelona" into locationInput
    And press button searchBtn
    And press button moreFiltersBtn
    And verify if popupModal exists
    And press button bathroomsIncreaseBtn
    And press button bathroomsIncreaseBtn
    And verify if bathroomsQuantityValue has text "2"
    And press button superhostBtn
    And verify if button superhostBtn checked
    And check checkbox gymCheckbox
    And verify if checkbox gymCheckbox checked
    And verify if button clearAllBtn enabled
    And press button clearAllBtn
    Then verify if bathroomsQuantityValue has text "0"
    Then verify if button superhostBtn unchecked
    Then verify if checkbox gymCheckbox unchecked

  @test
  Scenario: test#3 go to moreFilters modal and press show more amenities btn
    Given open airbnb.com
    When type text "Tokyo" into locationInput
    And press button searchBtn
    And press button moreFiltersBtn
    And verify if ironCheckbox not visible
    And press button showMoreAmenitiesBtn
    And check checkbox ironCheckbox
    And press button closeAmenitiesBtn
    And verify if ironCheckbox not visible
    And press button moreFiltersSubmitBtn
    Then verify if moreFiltersBtn has text "1"

  @test
  Scenario: test#4 go to Type of place modal and select different options
    Given open airbnb.com
    When type text "Ukraine" into locationInput
    And press button searchBtn
    And verify if button typeOfPlaceBtn not pressed
    And press btn typeOfPlaceBtn
    And verify if typeOfPlaceMenuPanel exists
    And verify if button clearBtn disabled
    And check checkbox privateRoomCheckbox
    And verify if button clearBtn enabled
    And press button clearBtn
    And verify if checkbox privateRoomCheckbox unchecked
    And check checkbox hotelRoomCheckbox
    And verify if checkbox hotelRoomCheckbox checked
    And press button filterPanelSaveButton
    Then verify if typeOfPlaceBtn has text "Hotel room"
    Then verify if button typeOfPlaceBtn pressed

  @test
  Scenario: test#5 go to Price modal and select different options
    Given open airbnb.com
    When type text "Belarus" into locationInput
    And press button searchBtn
    And verify if button priceBtn not pressed
    And press button priceBtn
    And verify if minPriceInput has value "10"
    And double-click on element minPriceInput
    And type text "66" into minPriceInput
    And verify if minPriceInput has value "66"
    And press button filterPanelSaveButton
    And verify if button priceBtn pressed
    And verify if priceBtn has text "$66+"
    And press button priceBtn
    And set focus to minPriceInput
    And press TAB key
    And verify if element maxPriceInput focused
    And type text "50" into maxPriceInput
    And verify if maxPriceInput has value "50"
    And set focus to minPriceInput
    And verify if minPriceInput has value "45"
    And press button filterPanelSaveButton
    Then verify if button priceBtn pressed
    Then verify if priceBtn has text "$45 - $50"
    And press button priceBtn
    And press button cleanButton
    And press button filterPanelSaveButton
    Then verify if button priceBtn not pressed
    Then verify if priceBtn has text "Price"


  @test
  Scenario: test#6 select some filter options and go to host page to compare
    Given open airbnb.com
    When type text "Miami" into locationInput
    And press button searchBtn
    And press button moreFiltersBtn
    And press button showMoreAmenitiesBtn
    And check checkbox airConditioningCheckbox
    And check checkbox poolCheckbox
    And check checkbox petsAllowed
    And press button moreFiltersSubmitBtn
    And verify if staysItemList exists
    And click on first element from the staysItemlist
    And switch to second browser tab
    And verify if current link has text "rooms"
    And wait about 10000ms
    Then verify if policiesSection has text "Pets are allowed"
    Then press button showAllAmenitiesBtn
    Then verify if popupModal exists
    Then verify if popupModal has text "Pool"
    Then verify if popupModal has text "Air conditioning"
    Then press button closeAmenitiesPopupBtn




