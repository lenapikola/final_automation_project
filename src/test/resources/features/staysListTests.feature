Feature: check stays list functionality on stays page

  @test
  Scenario: test#1 type a city name into location input and press search button
    Given open airbnb.com
    When type text "Berlin" into locationInput
    And press button searchBtn
    Then verify if title has text "Stays in Berlin"
    Then verify if littleSearchPanel exists
    Then verify if littleLocationSearchBtn has text "Berlin"