Feature: check map functionality on stays page

  @test(name='map#1')
  Scenario: test#1 go to stays page and press hide map button
    Given open airbnb.com
    When type text "Oslo" into locationInput
    And press button searchBtn
    And verify if mapPanel exists
    And press button closeMapBtn
    Then verify if mapPanel is not shown
    Then press button showMapBtn
    Then verify if mapPanel exists

  @test
  Scenario: test#2 go to map, set filter as i type on and zoom the map
    Given open airbnb.com
    When type text "Helsinki" into locationInput
    And press button searchBtn
    And verify if mapPanel exists
    And verify if checkbox searchAsIMoveTheMapCheckbox checked
    And get first name from the hosts list
    And press button zoomInBtn
    And press button zoomInBtn
    And press button zoomInBtn
    And wait about 6000ms
    And get first name from the hosts list again
    Then verify if names are not equal

  @test
  Scenario: test#3 go to map, set filter as i type off and zoom the map
    Given open airbnb.com
    When type text "Stockholm" into locationInput
    And press button searchBtn
    And check checkbox searchAsIMoveTheMapCheckbox
    And verify if checkbox searchAsIMoveTheMapCheckbox unchecked
    And get first name from the hosts list
    And press button zoomOutBtn
    And press button zoomOutBtn
    And press button zoomOutBtn
    And wait about 6000ms
    And get first name from the hosts list again
    Then verify if names are equal

  @test
  Scenario: test#4 go to map, set filter as i type off, zoom the map and press search this area button
    Given open airbnb.com
    When type text "Amsterdam" into locationInput
    And press button searchBtn
    And press button zoomInBtn
    And press button zoomInBtn
    And check checkbox searchAsIMoveTheMapCheckbox
    And press button zoomInBtn
    And press button zoomInBtn
    And press button zoomOutBtn
    And get first name from the hosts list
    And wait about 6000ms
    And verify if searchThisAreaBtn exists
    And press button searchThisAreaBtn
    And wait about 6000ms
    And get first name from the hosts list again
    Then verify if names are not equal

  @test
  Scenario: test#5 go to map and click on any preview thumbnail
    Given open airbnb.com
    When type text "Kopenhagen" into locationInput
    And press button searchBtn
    And verify if mapPanel exists
    And click on first element from the mapItemsList
    Then verify if mapPreviewThumbnail exists

  @test(name='map#6')
  Scenario: test#6 go to stays page and press add a place to the map button
    Given open airbnb.com
    When type text "Madrid" into locationInput
    And press button searchBtn
    And verify if mapPanel exists
    And press button addAPlaceToTheMapBtn
    And verify if findAPlaceInput exists
    And type text "museum" into findAPlaceInput
    And verify if findAPlaceSuggestionPanel exists
    And select first suggestion from findAPlaceSuggestionPanel
    Then verify if mapPreviewThumbnail exists
