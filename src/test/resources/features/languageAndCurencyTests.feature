Feature: check language and currency popup functionality on main page

  @test
  Scenario: test#1 go to language modal and select spanish language
    Given open airbnb.com
    When press button languageCurrencyBtn
    And verify if popupModal exists
    And press button spanishLanguageBtn
    Then verify if tabTitle has text "Alquileres vacacionales, alojamientos, experiencias y lugares - Airbnb"
    Then verify if languageName in footer has text "Español"

  @test
  Scenario: test#2 go to currency modal and select HRK currency
    Given open airbnb.com
    When press button languageCurrencyBtn
    And press button currencyBtn
    And select HRK from currencies list
    Then verify if currencyIcon in footer has text "kn"
    Then verify if currencyName in footer has text "HRK"
