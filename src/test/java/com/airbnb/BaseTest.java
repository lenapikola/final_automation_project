package com.airbnb;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;

import static com.codeborne.selenide.Selenide.open;

public class BaseTest {
    protected static final String BASE_URL = "https://airbnb.com/";
    protected static WebDriver driver;

    @BeforeClass
    public static void setup() {
        Configuration.remote = "http://localhost:4444/wd/hub";
        Configuration.driverManagerEnabled = false;
//        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/driver/chromedriver");
//        System.setProperty("selenide.browser", "Chrome");
//        System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/driver/geckodriver");
//        System.setProperty("selenide.browser", "Firefox");
        Configuration.startMaximized = true;
        Configuration.timeout = 10000;
        Configuration.reportsFolder = "test-result/reports";
    }

    @BeforeClass
    public static void openBaseUrl() throws InterruptedException {
        open(BASE_URL);
        Thread.sleep(3000);
    }

    @AfterClass
    public static void closeDriver() throws InterruptedException {
        Thread.sleep(3000);
        Selenide.closeWebDriver();
    }
}
