package com.airbnb.testngTests;

import com.airbnb.BaseTest;
import com.airbnb.pageObjects.MainPage;
import com.airbnb.pageObjects.StaysPage;
import org.junit.Test;

import static com.airbnb.pageObjects.BasePage.*;
import static com.airbnb.pageObjects.MainPage.*;

public class SearchPanelTests extends BaseTest {
    private MainPage mainPage = new MainPage(BaseTest.driver);
    private StaysPage staysPage = new StaysPage(BaseTest.driver);

    @Test
    public void user_can_search_by_city_name_only() {
        mainPage.typeLocation("Berlin");
        mainPage.pressBtn(SEARCH_BTN);
        staysPage.isTitleDisplayed();
        staysPage.isTitleHasText("Stays in Berlin");
        staysPage.isDisplayed(LITTLE_SEARCH_PANEL);
        staysPage.isLittleLocationSearchBtnHasText("Berlin");
    }

    @Test
    public void user_can_search_by_non_existent_location() {
        mainPage.typeLocation("Qerlin");
        mainPage.pressBtn(SEARCH_BTN);
        staysPage.isTravelTitleDisplayed();
        staysPage.isTravelTitleHasText("Travel the world with Airbnb");
    }

    @Test
    public void user_can_search_by_nearby_location() {
        mainPage.pressBtn(LOCATION_INPUT);
        mainPage.isDisplayed(EXPLORE_NEARBY_BTN);
        mainPage.pressBtn(EXPLORE_NEARBY_BTN);
        mainPage.pressBtn(SEARCH_BTN);
        staysPage.isTitleDisplayed();
        staysPage.isTitleHasText("Places to stay near you");
    }

    @Test
    public void user_can_search_by_all_parameters() {
        mainPage.typeLocation("Lima");
        mainPage.pressBtn(DATES_INPUT);
        mainPage.isDisplayed(DATES_PANEL);
        mainPage.selectCheckInDate();
        mainPage.selectCheckOutDate();
        mainPage.pressBtn(GUESTS_BTN);
        mainPage.isDisplayed(GUESTS_PANEL);
        mainPage.changeQuantity(ADULTS_INCREASE_BTN);
        mainPage.changeQuantity(ADULTS_INCREASE_BTN);
        mainPage.isElementHasText(ADULTS_QUANTITY_VALUE, "2");
        mainPage.pressBtn(SEARCH_BTN);
        staysPage.isTitleHasText("Stays in");
        staysPage.isTitleHasText("Lima");
        staysPage.isLittleLocationSearchBtnHasText("Lima");
        staysPage.isLittleGuestsSearchBtnHasValue("2");
    }

    @Test
    public void infants_are_not_counted_as_real_guests() {
        mainPage.typeLocation("Athens");
        mainPage.pressBtn(GUESTS_BTN);
        mainPage.changeQuantity(ADULTS_INCREASE_BTN);
        mainPage.isElementHasText(ADULTS_QUANTITY_VALUE, "1");
        mainPage.changeQuantity(INFANTS_INCREASE_BTN);
        mainPage.isElementHasText(INFANTS_QUANTITY_VALUE, "1");
        mainPage.pressBtn(SEARCH_BTN);
        staysPage.isLittleGuestsSearchBtnHasValue("1");
    }

    @Test
    public void user_can_not_select_children_without_any_adults() {
        mainPage.pressBtn(GUESTS_BTN);
        mainPage.isElementHasText(ADULTS_QUANTITY_VALUE, "0");
        mainPage.changeQuantity(INFANTS_INCREASE_BTN);
        mainPage.isElementHasText(INFANTS_QUANTITY_VALUE, "1");
        mainPage.isElementHasText(ADULTS_QUANTITY_VALUE, "1");
        mainPage.changeQuantity(ADULTS_DECREASE_BTN);
        mainPage.isElementHasText(INFANTS_QUANTITY_VALUE, "1");
        mainPage.isElementHasText(ADULTS_QUANTITY_VALUE, "1");
        mainPage.changeQuantity(INFANTS_DECREASE_BTN);
        mainPage.isElementHasText(INFANTS_QUANTITY_VALUE, "0");
        mainPage.isElementHasText(ADULTS_QUANTITY_VALUE, "1");
        mainPage.changeQuantity(ADULTS_DECREASE_BTN);
        mainPage.isElementHasText(ADULTS_QUANTITY_VALUE, "0");
        mainPage.changeQuantity(CHILDREN_INCREASE_BTN);
        mainPage.changeQuantity(CHILDREN_INCREASE_BTN);
        mainPage.isElementHasText(CHILDREN_QUANTITY_VALUE, "2");
        mainPage.isElementHasText(ADULTS_QUANTITY_VALUE, "1");
        mainPage.changeQuantity(ADULTS_DECREASE_BTN);
        mainPage.isElementHasText(CHILDREN_QUANTITY_VALUE, "2");
        mainPage.isElementHasText(ADULTS_QUANTITY_VALUE, "1");
    }

    @Test
    public void user_can_access_search_button_while_scrolling() {
        mainPage.scrollToView(FOOTER);
        mainPage.isDisplayed(LITTLE_SEARCH_PANEL);
        mainPage.pressBtn(LITTLE_SEARCH_BUTTON);
        mainPage.isDisplayed(DATES_PANEL);
        mainPage.isDisplayed(GUESTS_PANEL);
        mainPage.isDisplayed(LOCATION_INPUT);
        mainPage.isDisplayed(SEARCH_BTN);
    }
}
