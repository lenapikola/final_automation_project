package com.airbnb.testngTests;

import com.airbnb.BaseTest;
import com.airbnb.pageObjects.HostPage;
import com.airbnb.pageObjects.MainPage;
import com.airbnb.pageObjects.StaysPage;
import com.codeborne.selenide.Condition;
import org.junit.Test;

import static com.airbnb.pageObjects.HostPage.*;
import static com.airbnb.pageObjects.MainPage.*;
import static com.airbnb.pageObjects.StaysPage.*;
import static com.codeborne.selenide.Selenide.*;

public class MoreFiltersTests extends BaseTest {
    private MainPage mainPage = new MainPage(BaseTest.driver);
    private StaysPage staysPage = new StaysPage(BaseTest.driver);
    private HostPage hostPage = new HostPage(BaseTest.driver);

    @Test
    public void it_is_possible_to_apply_filters() {
        mainPage.typeLocation("Mexico");
        mainPage.pressBtn(SEARCH_BTN);
        staysPage.pressBtn(MORE_FILTERS_BTN);
        staysPage.isDisplayed(POPUP_MODAL);
        staysPage.changeQuantity(BEDS_INCREASE_BTN);
        staysPage.isElementHasText(BEDS_QUANTITY_VALUE, "1");
        staysPage.checkCheckbox(PETS_ALLOWED_CHECKBOX);
        staysPage.isElementHasType(POOL_CHECKBOX, "checkbox");
        staysPage.checkCheckbox(POOL_CHECKBOX);
        staysPage.pressBtn(SUPERHOST_BTN);
        staysPage.pressBtn(MORE_FILTERS_SUBMIT_BTN);
        staysPage.isDisplayed(STAYS_ITEM_LIST);
        staysPage.isElementHasValue(MORE_FILTERS_BTN, "4");
    }

    @Test
    public void it_is_possible_to_clear_all_filters() {
        mainPage.typeLocation("Barcelona");
        mainPage.pressBtn(SEARCH_BTN);
        staysPage.pressBtn(MORE_FILTERS_BTN);
        staysPage.changeQuantity(BATHROOMS_INCREASE_BTN);
        staysPage.changeQuantity(BATHROOMS_INCREASE_BTN);
        staysPage.isElementHasText(BATHROOMS_QUANTITY_VALUE, "2");
        staysPage.pressBtn(SUPERHOST_BTN);
        staysPage.isBtnChecked(SUPERHOST_BTN, true);
        staysPage.checkCheckbox(GYM_CHECKBOX);
        staysPage.isCheckboxChecked(GYM_CHECKBOX);
        staysPage.isBtnEnabled(CLEAR_ALL_FILTERS_BTN);
        staysPage.pressBtn(CLEAR_ALL_FILTERS_BTN);
        staysPage.isElementHasText(BATHROOMS_QUANTITY_VALUE, "0");
        staysPage.isBtnChecked(SUPERHOST_BTN, false);
        staysPage.isCheckboxUnchecked(GYM_CHECKBOX);
    }

    @Test
    public void it_is_possible_to_apply_expanded_options() {
        mainPage.typeLocation("Tokyo");
        mainPage.pressBtn(SEARCH_BTN);
        staysPage.pressBtn(MORE_FILTERS_BTN);
        staysPage.isCheckboxNotVisible(IRON_CHECKBOX);
        staysPage.pressBtn(SHOW_MORE_AMENITIES_BTN);
        staysPage.checkCheckbox(IRON_CHECKBOX);
        staysPage.pressBtn(CLOSE_AMENITIES_BTN);
        staysPage.isCheckboxNotVisible(IRON_CHECKBOX);
        staysPage.pressBtn(MORE_FILTERS_SUBMIT_BTN);
        staysPage.isElementHasText(MORE_FILTERS_BTN, "1");
    }

    @Test
    public void it_is_possible_to_filter_with_type_of_place_button() {
        mainPage.typeLocation("Ukraine");
        mainPage.pressBtn(SEARCH_BTN);
        staysPage.isBtnPressed(TYPE_OF_PLACE_BTN, false);
        staysPage.pressBtn(TYPE_OF_PLACE_BTN);
        staysPage.isDisplayed(TYPE_OF_PLACE_MENU_PANEL);
        staysPage.isBtnDisabled(FILTER_PANEL_CLEAR_BTN);
        staysPage.checkCheckbox(PRIVATE_ROOM_CHECKBOX);
        staysPage.isCheckboxChecked(PRIVATE_ROOM_CHECKBOX);
        staysPage.isBtnEnabled(FILTER_PANEL_CLEAR_BTN);
        staysPage.pressBtn(FILTER_PANEL_CLEAR_BTN);
        staysPage.isCheckboxUnchecked(PRIVATE_ROOM_CHECKBOX);
        staysPage.checkCheckbox(HOTEL_ROOM_CHECKBOX);
        staysPage.isCheckboxChecked(HOTEL_ROOM_CHECKBOX);
        staysPage.pressBtn(FILTER_PANEL_SAVE_BTN);
        staysPage.isElementHasText(TYPE_OF_PLACE_BTN, "Hotel room");
        staysPage.isBtnPressed(TYPE_OF_PLACE_BTN, true);
    }

    @Test
    public void it_is_possible_to_filter_with_price_button() {
        mainPage.typeLocation("Belarus");
        mainPage.pressBtn(SEARCH_BTN);
        staysPage.isBtnPressed(PRICE_BTN, false);
        staysPage.pressBtn(PRICE_BTN);
        staysPage.doubleClickOnElement(MIN_PRICE_INPUT);
        staysPage.typeTextInto(MIN_PRICE_INPUT, "66");
        staysPage.isElementHasValue(MIN_PRICE_INPUT, "66");
        staysPage.pressBtn(FILTER_PANEL_SAVE_BTN);
        staysPage.isBtnPressed(PRICE_BTN, true);
        staysPage.isElementHasText(PRICE_BTN, "$66+");
        staysPage.pressBtn(PRICE_BTN);
        staysPage.pressBtn(MIN_PRICE_INPUT);
        staysPage.tabToNextElementFrom(MIN_PRICE_INPUT);
        staysPage.isElementFocused(MAX_PRICE_INPUT);
        staysPage.typeTextInto(MAX_PRICE_INPUT, "50");
        staysPage.isElementHasValue(MAX_PRICE_INPUT, "50");
        staysPage.pressBtn(MIN_PRICE_INPUT);
        staysPage.isElementHasValue(MIN_PRICE_INPUT, "45");
        staysPage.pressBtn(FILTER_PANEL_SAVE_BTN);
        staysPage.isBtnPressed(PRICE_BTN, true);
        staysPage.isElementHasText(PRICE_BTN, "$45 - $50");
        staysPage.pressBtn(PRICE_BTN);
        staysPage.pressBtn(FILTER_PANEL_CLEAR_BTN);
        staysPage.pressBtn(FILTER_PANEL_SAVE_BTN);
        staysPage.isBtnPressed(PRICE_BTN, false);
        staysPage.isElementHasText(PRICE_BTN, "Price");
    }

    @Test
    public void test_selected_filters_are_shown_on_host_page() throws InterruptedException {
        mainPage.typeLocation("Miami");
        mainPage.pressBtn(SEARCH_BTN);
        staysPage.pressBtn(MORE_FILTERS_BTN);
        staysPage.pressBtn(SHOW_MORE_AMENITIES_BTN);
        staysPage.checkCheckbox(AIR_CONDITIONING_CHECKBOX);
        staysPage.checkCheckbox(POOL_CHECKBOX);
        staysPage.checkCheckbox(PETS_ALLOWED_CHECKBOX);
        staysPage.checkCheckbox(ENGLISH_CHECKBOX);
        staysPage.pressBtn(MORE_FILTERS_SUBMIT_BTN);
        staysPage.isDisplayed(STAYS_ITEM_LIST);
        staysPage.getFirstElementFromTheList(STAYS_ITEM_LIST);
        staysPage.switchToBrowserTab(1);
        hostPage.isCurrentLinkContainsText("rooms");
        hostPage.waitUntilElementIsVisible(HOST_CONTACT_INFO);
        hostPage.isElementHasText(HOST_CONTACT_INFO, "English");
        $$(POLICIES_SECTIONS).get(0).shouldHave(Condition.textCaseSensitive("Pets are allowed"));
        hostPage.pressBtn(SHOW_ALL_AMENITIES_BTN);
        hostPage.isDisplayed(POPUP_MODAL);
        hostPage.isElementHasText(POPUP_MODAL, "Pool");
        hostPage.isElementHasText(POPUP_MODAL, "Air conditioning");
        hostPage.pressBtn(CLOSE_AMENITIES_POPUP_BTN);
    }
}
