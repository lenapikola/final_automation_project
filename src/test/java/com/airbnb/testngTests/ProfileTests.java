package com.airbnb.testngTests;

import com.airbnb.BaseTest;
import com.airbnb.pageObjects.MainPage;
import org.junit.Test;

import static com.airbnb.pageObjects.BasePage.*;

public class ProfileTests extends BaseTest {

    private MainPage mainPage = new MainPage(BaseTest.driver);

    private String user_login_email = "jilado2932@95ta.com";
    private String user_login_password = "cucumber00";

    @Test
    public void registered_user_can_log_in() {
        mainPage.pressBtn(PROFILE_BTN);
        mainPage.isDisplayed(PROFILE_MENU);
        mainPage.pressBtn(LOGIN_BTN);
        mainPage.isDisplayed(POPUP_MODAL);
        mainPage.pressBtn(LOGIN_WITH_EMAIL_BTN);
        mainPage.typeLoginEmail(user_login_email);
        mainPage.typeLoginPassword(user_login_password);
        mainPage.pressBtn(SUBMIT_LOGIN_BTN);
        // i'm not a robot check here
    }
}
