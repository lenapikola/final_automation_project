package com.airbnb.testngTests;

import com.airbnb.BaseTest;
import com.airbnb.pageObjects.MainPage;
import com.airbnb.pageObjects.StaysPage;
import com.codeborne.selenide.Selenide;
import org.junit.Test;

import static com.airbnb.pageObjects.BasePage.*;

public class LanguageAndCurrencyTests extends BaseTest {
    private MainPage mainPage = new MainPage(BaseTest.driver);
    private StaysPage staysPage = new StaysPage(BaseTest.driver);

    @Test
    public void it_is_possible_to_change_language() {
        mainPage.pressBtn(LANGUAGE_CURRENCY_BTN);
        mainPage.isDisplayed(POPUP_MODAL);
        mainPage.selectSpanishFromLanguages();
        Selenide.title().equals("Alquileres vacacionales, alojamientos, experiencias y lugares - Airbnb");
        mainPage.isFooterLanguageNameEqualsText("Español");
    }

    @Test
    public void it_is_possible_to_change_currency() {
        mainPage.pressBtn(LANGUAGE_CURRENCY_BTN);
        mainPage.pressBtn(CURRENCY_BTN);
        mainPage.selectHRKFromCurrencies();
        mainPage.isFooterCurrencyIconEqualsValue("kn");
        mainPage.isFooterCurrencyNameEqualsText("HRK");
    }
}
