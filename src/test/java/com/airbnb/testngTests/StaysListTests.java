package com.airbnb.testngTests;

import com.airbnb.BaseTest;
import com.airbnb.pageObjects.HostPage;
import com.airbnb.pageObjects.MainPage;
import com.airbnb.pageObjects.StaysPage;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

import static com.airbnb.pageObjects.MainPage.SEARCH_BTN;
import static com.airbnb.pageObjects.StaysPage.*;

public class StaysListTests extends BaseTest {
    private MainPage mainPage = new MainPage(BaseTest.driver);
    private StaysPage staysPage = new StaysPage(BaseTest.driver);
    private HostPage hostPage = new HostPage(BaseTest.driver);

    @Test
    public void user_can_navigate_through_photo_previews() {
        mainPage.typeLocation("Buenos Aires");
        mainPage.pressBtn(SEARCH_BTN);
        staysPage.isDisplayed(STAYS_ITEM_LIST);
        staysPage.isDisplayed(ITEM_PREVIEW_IMAGE);
        staysPage.hoverOverElement(STAYS_ITEM_LIST);
        File img1 = staysPage.getScreenshotOfElement(ITEM_PREVIEW_IMAGE);
        staysPage.isDisplayed(NEXT_IMAGE_BTN);
        staysPage.pressBtn(NEXT_IMAGE_BTN);
        File img2 = staysPage.getScreenshotOfElement(ITEM_PREVIEW_IMAGE);
        staysPage.pressBtn(NEXT_IMAGE_BTN);
        File img3 = staysPage.getScreenshotOfElement(ITEM_PREVIEW_IMAGE);
        Assert.assertNotEquals(img1, img2);
        Assert.assertNotEquals(img2, img3);
    }

//    @Test
//    public void user_can_navigate_through_list_pages_by_next_button() {
//        mainPage.typeLocation("Rio");
//        mainPage.clickBtn(SEARCH_BTN);
//        staysPage.isDisplayed(STAYS_ITEM_LIST);
//    }
//
//    @Test
//    public void user_can_navigate_through_list_pages_by_page_number_button() {
//
//    }
//
//    @Test
//    public void user_can_navigate_through_cities_list_previews_by_arrow_button() {
//
//    }
//
//    @Test
//    public void responded_location_is_pointed_on_map_on_list_item_hovering() {
//
//    }
}
