package com.airbnb.testngTests;

import com.airbnb.BaseTest;
import com.airbnb.pageObjects.MainPage;
import com.airbnb.pageObjects.StaysPage;
import org.junit.Assert;
import org.junit.Test;

import static com.airbnb.pageObjects.MainPage.SEARCH_BTN;
import static com.airbnb.pageObjects.StaysPage.*;

public class MapTests extends BaseTest {

    private MainPage mainPage = new MainPage(BaseTest.driver);
    private StaysPage staysPage = new StaysPage(BaseTest.driver);

    @Test
    public void user_can_close_the_map_panel_with_x_button() {
        mainPage.typeLocation("Oslo");
        mainPage.pressBtn(SEARCH_BTN);
        staysPage.isDisplayed(MAP_PANEL);
        staysPage.pressBtn(CLOSE_MAP_BTN);
        staysPage.isNotDisplayed(MAP_PANEL);
        staysPage.pressBtn(SHOW_MAP_BTN);
        staysPage.isDisplayed(MAP_PANEL);
    }

    @Test
    public void hosts_list_updated_while_search_as_i_move_is_on() throws InterruptedException {
        mainPage.typeLocation("Helsinki");
        mainPage.pressBtn(SEARCH_BTN);
        staysPage.isDisplayed(MAP_PANEL);
        staysPage.isCheckboxChecked(SEARCH_AS_I_MOVE_THE_MAP_CHECKBOX);
        String host1_name = staysPage.getFirstHostNameFromTheList(STAYS_ITEM_LIST);
        staysPage.pressBtn(ZOOM_IN_MAP_BTN);
        staysPage.pressBtn(ZOOM_IN_MAP_BTN);
        staysPage.pressBtn(ZOOM_IN_MAP_BTN);
        Thread.sleep(5000);
        String host2_name = staysPage.getFirstHostNameFromTheList(STAYS_ITEM_LIST);
        System.out.println(host1_name);
        System.out.println(host2_name);
        Assert.assertNotEquals(host1_name, host2_name);
    }

    @Test
    public void hosts_list_not_updated_while_search_as_i_move_is_off() throws InterruptedException {
        mainPage.typeLocation("Stockholm");
        mainPage.pressBtn(SEARCH_BTN);
        staysPage.isDisplayed(MAP_PANEL);
        staysPage.checkCheckbox(SEARCH_AS_I_MOVE_THE_MAP_CHECKBOX);
        staysPage.isCheckboxUnchecked(SEARCH_AS_I_MOVE_THE_MAP_CHECKBOX);
        String host1_name = staysPage.getFirstHostNameFromTheList(STAYS_ITEM_LIST);
        staysPage.pressBtn(ZOOM_OUT_MAP_BTN);
        staysPage.pressBtn(ZOOM_OUT_MAP_BTN);
        staysPage.pressBtn(ZOOM_OUT_MAP_BTN);
        String host2_name = staysPage.getFirstHostNameFromTheList(STAYS_ITEM_LIST);
        System.out.println(host1_name);
        System.out.println(host2_name);
        Assert.assertEquals(host1_name, host2_name);
    }

    @Test
    public void user_can_update_hosts_list_with_search_this_area_button() throws InterruptedException {
        mainPage.typeLocation("Amsterdam");
        mainPage.pressBtn(SEARCH_BTN);
        staysPage.pressBtn(ZOOM_IN_MAP_BTN);
        staysPage.pressBtn(ZOOM_IN_MAP_BTN);
        staysPage.checkCheckbox(SEARCH_AS_I_MOVE_THE_MAP_CHECKBOX);
        staysPage.pressBtn(ZOOM_IN_MAP_BTN);
        staysPage.pressBtn(ZOOM_IN_MAP_BTN);
        staysPage.pressBtn(ZOOM_OUT_MAP_BTN);
        String host1_name = staysPage.getFirstHostNameFromTheList(STAYS_ITEM_LIST);
        staysPage.isDisplayed(SEARCH_THIS_AREA_BTN);
        staysPage.pressBtn(SEARCH_THIS_AREA_BTN);
        Thread.sleep(5000);
        String host2_name = staysPage.getFirstHostNameFromTheList(STAYS_ITEM_LIST);
        System.out.println(host1_name);
        System.out.println(host2_name);
        Assert.assertNotEquals(host1_name, host2_name);
    }

    @Test
    public void user_can_preview_host_thumbnail_from_map() {
        mainPage.typeLocation("Kopenhagen");
        mainPage.pressBtn(SEARCH_BTN);
        staysPage.isDisplayed(MAP_PANEL);
        staysPage.getFirstElementFromTheList(MAP_PIN_ITEM);
        staysPage.isDisplayed(MAP_PREVIEW_THUMBNAIL);
    }

    @Test
    public void user_can_add_a_place_to_the_map_button() {
        mainPage.typeLocation("Madrid");
        mainPage.pressBtn(SEARCH_BTN);
        staysPage.isDisplayed(MAP_PANEL);
        staysPage.pressBtn(ADD_A_PLACE_TO_THE_MAP_BTN);
        staysPage.isDisplayed(FIND_A_PLACE_INPUT);
        staysPage.typeTextInto(FIND_A_PLACE_INPUT, "museum");
        staysPage.isDisplayed(FIND_A_PLACE_SEARCH_SUGGESTIONS_PANEL);
        staysPage.pressBtn(FIRST_FIND_A_PLACE_SUGGESTION);
        staysPage.isNotDisplayed(FIND_A_PLACE_SEARCH_SUGGESTIONS_PANEL);
        staysPage.isDisplayed(MAP_PREVIEW_THUMBNAIL);
    }

}
