package com.airbnb.stepdefs;

import com.airbnb.pageObjects.HostPage;
import com.airbnb.pageObjects.MainPage;
import com.airbnb.pageObjects.StaysPage;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;

import static com.airbnb.pageObjects.BasePage.LITTLE_SEARCH_PANEL;
import static com.airbnb.pageObjects.HostPage.*;
import static com.airbnb.pageObjects.MainPage.*;
import static com.airbnb.pageObjects.StaysPage.*;
import static com.codeborne.selenide.Selenide.*;

public class MyStepdefs {
    public static WebDriver driver;
    String baseURL = "https://airbnb.com/";
    MainPage mainPage = new MainPage(driver);
    StaysPage staysPage = new StaysPage(driver);
    HostPage hostPage = new HostPage(driver);



    @Given("open airbnb.com")
    public void openAirbnbCom() {
        Configuration.remote = "http://localhost:4444/wd/hub";
        Configuration.driverManagerEnabled = false;
        System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/driver/geckodriver");
        System.setProperty("selenide.browser", "Firefox");
        Configuration.startMaximized = true;
        Configuration.timeout = 10000;
        Configuration.reportsFolder = "test-result/reports";
        open(baseURL);
    }


    @When("^type text \"([^\"]*)\" into locationInput$")
    public void typeTextIntoLocationInput(String arg0) { mainPage.typeLocation(arg0); }

    @And("^press button searchBtn$")
    public void pressButtonSearchBtn() {
        mainPage.pressBtn(SEARCH_BTN);
    }

    @Then("^verify if title has text \"([^\"]*)\"$")
    public void verifyIfTitleHasText(String arg1) {
        staysPage.isTitleHasText(arg1);
    }

    @Then("^verify that element littleSearchPanel exists$")
    public void verifyThatElementLittleSearchPanelExists() {
        staysPage.isDisplayed(LITTLE_SEARCH_PANEL);
    }

    @Then("^verify if littleLocationSearchBtn has text \"([^\"]*)\"$")
    public void verifyIfLittleLocationSearchBtnHasText(String arg2) {
        staysPage.isLittleLocationSearchBtnHasText(arg2);
    }

    @Then("verify if travel page title has text {string}")
    public void verifyIfTravelPageTitleHasText(String arg0) {
        staysPage.isTravelTitleHasText(arg0);
    }

    @And("verify if exploreNearbyBtn exists")
    public void verifyIfExploreNearbyBtnExists() { mainPage.isDisplayed(EXPLORE_NEARBY_BTN); }

    @And("press button exploreNearbyBtn")
    public void pressButtonExploreNearbyBtn() { mainPage.pressBtn(EXPLORE_NEARBY_BTN); }

    @Then("verify if littleSearchPanel exists")
    public void verifyIfLittleSearchPanelExists() { staysPage.isDisplayed(LITTLE_SEARCH_PANEL); }

    @And("press button datesInput")
    public void pressButtonDatesInput() { mainPage.pressBtn(DATES_INPUT); }

    @And("verify if datesPanel exists")
    public void verifyIfDatesPanelExists() { mainPage.isDisplayed(DATES_PANEL); }

    @And("select checkIn date")
    public void selectCheckInDate() { mainPage.selectCheckInDate(); }

    @And("select checkOut date")
    public void selectCheckOutDate() { mainPage.selectCheckOutDate(); }

    @And("press button guestsBtn")
    public void pressButtonGuestsBtn() { mainPage.pressBtn(GUESTS_BTN); }

    @And("verify if guestsPanel exists")
    public void verifyIfGuestsPanelExists() {
        mainPage.isDisplayed(GUESTS_PANEL);
    }

    @And("press button adultsIncreaseBtn")
    public void pressButtonAdultsIncreaseBtn() {
        mainPage.pressBtn(ADULTS_INCREASE_BTN);
    }

    @And("verify if adultsQuantityValue has text {string}")
    public void verifyIfAdultsQuantityValueHasText(String arg0) {
        mainPage.isElementHasText(ADULTS_QUANTITY_VALUE, arg0);
    }

    @Then("verify if littleGuestsBtn has value {string}")
    public void verifyIfLittleGuestsBtnHasValue(String arg0) {
        $$(LITTLE_SEARCH_BUTTON).get(2).shouldHave(Condition.text(arg0));
    }

    @And("press button infantsIncreaseBtn")
    public void pressButtonInfantsIncreaseBtn() {
        mainPage.pressBtn(INFANTS_INCREASE_BTN);
    }

    @And("verify if infantsQuantityValue has text {string}")
    public void verifyIfInfantsQuantityValueHasText(String arg0) {
        mainPage.isElementHasText(INFANTS_QUANTITY_VALUE, arg0);
    }

    @And("press button adultsDecreaseBtn")
    public void pressButtonAdultsDecreaseBtn() {
        mainPage.pressBtn(ADULTS_DECREASE_BTN);
    }

    @And("press button infantsDecreaseBtn")
    public void pressButtonInfantsDecreaseBtn() {
        mainPage.pressBtn(INFANTS_DECREASE_BTN);
    }

    @And("press button childrenIncreaseBtn")
    public void pressButtonChildrenIncreaseBtn() {
        mainPage.pressBtn(CHILDREN_INCREASE_BTN);
    }

    @And("verify if childrenQuantityValue has text {string}")
    public void verifyIfChildrenQuantityValueHasText(String arg0) {
        mainPage.isElementHasText(CHILDREN_QUANTITY_VALUE, arg0);
    }

    @When("scroll page to view footer")
    public void scrollPageToViewFooter() {
        $(FOOTER).scrollIntoView(true);
    }

    @And("press button littleSearchBtn")
    public void pressButtonLittleSearchBtn() {
        mainPage.pressBtn(LITTLE_SEARCH_BUTTON);
    }

    @Then("verify if locationInput exists")
    public void verifyIfLocationInputExists() {
        mainPage.isDisplayed(LOCATION_INPUT);
    }

    @Then("verify if searchBtn exists")
    public void verifyIfSearchBtnExists() {
        mainPage.isDisplayed(SEARCH_BTN);
    }

    @When("press button profileBtn")
    public void pressButtonProfileBtn() {
        mainPage.pressBtn(PROFILE_BTN);
    }

    @And("verify if profileMenu exists")
    public void verifyIfProfileMenuExists() {
        mainPage.isDisplayed(PROFILE_MENU);
    }

    @And("press button loginBtn")
    public void pressButtonLoginBtn() {
        mainPage.pressBtn(LOGIN_BTN);
    }

    @And("verify if popupModal exists")
    public void verifyIfPopupModalExists() {
        mainPage.isDisplayed(POPUP_MODAL);
    }

    @And("press button loginWithEmailBtn")
    public void pressButtonLoginWithEmailBtn() {
        mainPage.pressBtn(LOGIN_WITH_EMAIL_BTN);
    }

    @Then("press button submitLoginBtn")
    public void pressButtonSubmitLoginBtn() {
        mainPage.pressBtn(SUBMIT_LOGIN_BTN);
    }

    @When("press button languageCurrencyBtn")
    public void pressButtonLanguageCurrencyBtn() {
        mainPage.pressBtn(LANGUAGE_CURRENCY_BTN);
    }

    @And("press button spanishLanguageBtn")
    public void pressButtonSpanishLanguageBtn() {
        $$(SPANISH_LANGUAGE_BTNS).get(4).click();
    }

    @Then("verify if tabTitle has text {string}")
    public void verifyIfTabTitleHasText(String arg0) {
        Selenide.title().equals(arg0);
    }

    @Then("verify if languageName in footer has text {string}")
    public void verifyIfLanguageNameInFooterHasText(String arg0) {
        $$(SELECTED_LANG_CURR_NAME).get(0).shouldHave(Condition.text(arg0));
    }

    @And("press button currencyBtn")
    public void pressButtonCurrencyBtn() {
        mainPage.pressBtn(CURRENCY_BTN);
    }

    @And("select HRK from currencies list")
    public void selectHRKFromCurrenciesList() {
        $$(CURRENCIES_LIST).get(8).click();
    }

    @Then("verify if currencyIcon in footer has text {string}")
    public void verifyIfCurrencyIconInFooterHasText(String arg0) {
        $$(SELECTED_LANG_CURR_ICON).get(1).shouldHave(Condition.text(arg0));
    }

    @Then("verify if currencyName in footer has text {string}")
    public void verifyIfCurrencyNameInFooterHasText(String arg0) {
        $$(SELECTED_LANG_CURR_NAME).get(1).shouldHave(Condition.text(arg0));
    }

    @And("press button moreFiltersBtn")
    public void pressButtonMoreFilters() {
        staysPage.pressBtn(MORE_FILTERS_BTN);
    }

    @And("press button bedsIncreaseBtn")
    public void pressButtonBedsIncreaseBtn() {
        staysPage.changeQuantity(BEDS_INCREASE_BTN);
    }

    @And("verify if bedsQuantityValue has text {string}")
    public void verifyIfBedsQuantityValueHasText(String arg0) {
        staysPage.isElementHasText(BEDS_QUANTITY_VALUE, arg0);
    }

    @And("check checkbox petsAllowed")
    public void checkCheckboxPetsAllowed() {
        staysPage.checkCheckbox(PETS_ALLOWED_CHECKBOX);
    }

    @And("verify if element poolCheckbox has type {string}")
    public void verifyIfElementPoolCheckboxHasType(String arg0) {
        staysPage.isElementHasType(POOL_CHECKBOX, arg0);
    }

    @And("check checkbox poolCheckbox")
    public void checkCheckboxPoolCheckbox() {
        staysPage.checkCheckbox(POOL_CHECKBOX);
    }

    @And("press button superhostBtn")
    public void pressButtonSuperhostBtn() {
        staysPage.pressBtn(SUPERHOST_BTN);
    }

    @And("press button moreFiltersSubmitBtn")
    public void pressButtonMoreFiltersSubmitBtn() {
        staysPage.pressBtn(MORE_FILTERS_SUBMIT_BTN);
    }

    @Then("verify if staysItemList exists")
    public void verifyIfStaysItemListExists() {
        staysPage.isDisplayed(STAYS_ITEM_LIST);
    }

    @And("press button bathroomsIncreaseBtn")
    public void pressButtonBathroomsIncreaseBtn() {
        staysPage.pressBtn(BATHROOMS_INCREASE_BTN);
    }

    @And("verify if bathroomsQuantityValue has text {string}")
    public void verifyIfBathroomsQuantityValueHasText(String arg0) {
        $(BATHROOMS_QUANTITY_VALUE).shouldHave(Condition.text(arg0));
    }

    @And("verify if button superhostBtn checked")
    public void verifyIfButtonSuperhostBtnChecked() {
        staysPage.isBtnChecked(SUPERHOST_BTN, true);
    }

    @And("check checkbox gymCheckbox")
    public void checkCheckboxGymCheckbox() {
        staysPage.checkCheckbox(GYM_CHECKBOX);
    }

    @And("verify if checkbox gymCheckbox checked")
    public void verifyIfCheckboxGymCheckboxChecked() {
        staysPage.isCheckboxChecked(GYM_CHECKBOX);
    }

    @And("verify if button clearAllBtn enabled")
    public void verifyIfButtonClearAllBtnEnabled() {
        staysPage.isBtnEnabled(CLEAR_ALL_FILTERS_BTN);
    }

    @And("press button clearAllBtn")
    public void pressButtonClearAllBtn() {
        staysPage.pressBtn(CLEAR_ALL_FILTERS_BTN);
    }

    @Then("verify if button superhostBtn unchecked")
    public void verifyIfButtonSuperhostBtnUnchecked() {
        staysPage.isBtnChecked(SUPERHOST_BTN, false);
    }

    @Then("verify if checkbox gymCheckbox unchecked")
    public void verifyIfCheckboxGymCheckboxUnchecked() {
        staysPage.isCheckboxUnchecked(GYM_CHECKBOX);
    }

    @And("press button showMoreAmenitiesBtn")
    public void pressButtonShowMoreAmenitiesBtn() {
        staysPage.pressBtn(SHOW_MORE_AMENITIES_BTN);
    }

    @And("check checkbox ironCheckbox")
    public void checkCheckboxIronCheckbox() {
        staysPage.checkCheckbox(IRON_CHECKBOX);
    }

    @And("press button closeAmenitiesBtn")
    public void pressButtonCloseAmenitiesBtn() {
        staysPage.pressBtn(CLOSE_AMENITIES_BTN);
    }

    @And("verify if ironCheckbox not visible")
    public void verifyIfIronCheckboxNotVisible() {
        staysPage.isCheckboxNotVisible(IRON_CHECKBOX);
    }

    @And("verify if button typeOfPlaceBtn pressed")
    public void verifyIfButtonTypeOfPlaceBtnPressed() {
        staysPage.isBtnPressed(TYPE_OF_PLACE_BTN, true);
    }

    @And("press btn typeOfPlaceBtn")
    public void pressBtnTypeOfPlaceBtn() {
        staysPage.pressBtn(TYPE_OF_PLACE_BTN);
    }

    @And("verify if typeOfPlaceMenuPanel exists")
    public void verifyIfTypeOfPlaceMenuPanelExists() {
        staysPage.isDisplayed(TYPE_OF_PLACE_MENU_PANEL);
    }

    @And("verify if button clearBtn disabled")
    public void verifyIfButtonClearBtnDisabled() {
        staysPage.isBtnDisabled(FILTER_PANEL_CLEAR_BTN);
    }

    @And("check checkbox privateRoomCheckbox")
    public void checkCheckboxPrivateRoomCheckbox() {
        staysPage.checkCheckbox(PRIVATE_ROOM_CHECKBOX);
    }

    @And("verify if button clearBtn enabled")
    public void verifyIfButtonClearBtnEnabled() {
        staysPage.isBtnEnabled(FILTER_PANEL_CLEAR_BTN);
    }

    @And("press button clearBtn")
    public void pressButtonClearBtn() {
        staysPage.pressBtn(FILTER_PANEL_CLEAR_BTN);
    }

    @And("verify if checkbox privateRoomCheckbox unchecked")
    public void verifyIfCheckboxPrivateRoomCheckboxUnchecked() {
        staysPage.isCheckboxUnchecked(PRIVATE_ROOM_CHECKBOX);
    }

    @And("check checkbox hotelRoomCheckbox")
    public void checkCheckboxHotelRoomCheckbox() {
        staysPage.checkCheckbox(HOTEL_ROOM_CHECKBOX);
    }

    @And("verify if checkbox hotelRoomCheckbox checked")
    public void verifyIfCheckboxHotelRoomCheckboxChecked() {
        staysPage.isCheckboxChecked(HOTEL_ROOM_CHECKBOX);
    }

    @And("press button filterPanelSaveButton")
    public void pressButtonFilterPanelSaveButton() {
        staysPage.pressBtn(FILTER_PANEL_SAVE_BTN);
    }

    @Then("verify if typeOfPlaceBtn has text {string}")
    public void verifyIfTypeOfPlaceBtnHasText(String arg0) {
        staysPage.isElementHasText(TYPE_OF_PLACE_BTN, arg0);
    }

    @And("verify if button priceBtn pressed")
    public void verifyIfButtonPriceBtnPressed() {
        staysPage.isBtnPressed(PRICE_BTN, true);
    }

    @And("verify if minPriceInput has value {string}")
    public void verifyIfMinPriceInputHasValue(String arg0) {
        staysPage.isElementHasValue(MIN_PRICE_INPUT, arg0);
    }

    @And("double-click on element minPriceInput")
    public void doubleClickOnElementMinPriceInput() {
        staysPage.doubleClickOnElement(MIN_PRICE_INPUT);
    }

    @And("type text {string} into minPriceInput")
    public void typeTextIntoMinPriceInput(String arg0) {
        staysPage.typeTextInto(MIN_PRICE_INPUT, arg0);
    }

    @And("verify if priceBtn has text {string}")
    public void verifyIfPriceBtnHasText(String arg0) {
        staysPage.isElementHasText(PRICE_BTN, arg0);
    }

    @And("press button priceBtn")
    public void pressButtonPriceButton() {
        $(PRICE_BTN).click();
    }

    @And("set focus to minPriceInput")
    public void setFocusToMinPriceInput() { staysPage.pressBtn(MIN_PRICE_INPUT); }

    @And("press TAB key")
    public void pressTABKey() { $(MIN_PRICE_INPUT).pressTab(); }

    @And("verify if element maxPriceInput focused")
    public void verifyIfElementMaxPriceInputFocused() {
        staysPage.isElementFocused(MAX_PRICE_INPUT);
    }

    @And("type text {string} into maxPriceInput")
    public void typeTextIntoMaxPriceInput(String arg0) {
        staysPage.typeTextInto(MAX_PRICE_INPUT, arg0);
    }

    @And("verify if maxPriceInput has value {string}")
    public void verifyIfMaxPriceInputHasValue(String arg0) {
        staysPage.isElementHasValue(MAX_PRICE_INPUT, arg0);
    }

    @And("press button cleanButton")
    public void pressButtonCleanButton() {
        staysPage.pressBtn(FILTER_PANEL_CLEAR_BTN);
    }

    @Then("verify if button priceBtn not pressed")
    public void verifyIfButtonPriceBtnNotPressed() { staysPage.isBtnPressed(PRICE_BTN, false); }

    @And("check checkbox airConditioningCheckbox")
    public void checkCheckboxAirConditioningCheckbox() { staysPage.checkCheckbox(AIR_CONDITIONING_CHECKBOX); }

    @And("check checkbox englishCheckbox")
    public void checkCheckboxEnglishCheckbox() { staysPage.checkCheckbox(ENGLISH_CHECKBOX); }

    @And("click on first element from the staysItemlist")
    public void clickOnFirstElementFromTheStaysItemlist() { staysPage.getFirstElementFromTheList(STAYS_ITEM_LIST); }

    @And("switch to second browser tab")
    public void switchToSecondBrowserTab() { staysPage.switchToBrowserTab(1); }

    @And("verify if current link has text {string}")
    public void verifyIfCurrentLinkHasText(String arg0) { staysPage.isCurrentLinkContainsText(arg0); }

    @And("press button showAllAmenitiesBtn")
    public void pressButtonShowAllAmenitiesBtn() { hostPage.pressBtn(SHOW_ALL_AMENITIES_BTN); }

    @Then("verify if popupModal has text {string}")
    public void verifyIfPopupModalHasText(String arg0) { hostPage.isElementHasText(POPUP_MODAL, arg0); }

    @Then("press button closeAmenitiesPopupBtn")
    public void pressButtonCloseAmenitiesPopupBtn() { hostPage.pressBtn(CLOSE_AMENITIES_POPUP_BTN); }

    @Then("verify if policiesSection has text {string}")
    public void verifyIfPoliciesSectionHasText(String arg0) {
        $$(POLICIES_SECTIONS).get(0).shouldHave(Condition.textCaseSensitive(arg0));
    }

    @And("verify if mapPanel exists")
    public void verifyIfMapPanelExists() { staysPage.isDisplayed(MAP_PANEL); }

    @And("press button closeMapBtn")
    public void pressButtonCloseMapBtn() { staysPage.pressBtn(CLOSE_MAP_BTN); }

    @Then("verify if mapPanel is not shown")
    public void verifyIfMapPanelIsNotShown() { staysPage.isNotDisplayed(MAP_PANEL);}

    @Then("press button showMapBtn")
    public void pressButtonShowMapBtn() { staysPage.pressBtn(SHOW_MAP_BTN); }

    @And("verify if checkbox searchAsIMoveTheMapCheckbox checked")
    public void verifyIfCheckboxSearchAsIMoveTheMapCheckboxChecked() {
        staysPage.isCheckboxChecked(SEARCH_AS_I_MOVE_THE_MAP_CHECKBOX);
    }

    @And("press button zoomInBtn")
    public void pressButtonZoomInBtn() { staysPage.pressBtn(ZOOM_IN_MAP_BTN); }

    @And("wait about {int}ms")
    public void waitAboutMs(int arg0) throws InterruptedException { Thread.sleep(arg0); }

    @And("check checkbox searchAsIMoveTheMapCheckbox")
    public void checkCheckboxSearchAsIMoveTheMapCheckbox() {
        staysPage.checkCheckbox(SEARCH_AS_I_MOVE_THE_MAP_CHECKBOX);
    }

    @And("verify if checkbox searchAsIMoveTheMapCheckbox unchecked")
    public void verifyIfCheckboxSearchAsIMoveTheMapCheckboxUnchecked() {
        staysPage.isCheckboxUnchecked(SEARCH_AS_I_MOVE_THE_MAP_CHECKBOX);
    }

    @And("press button zoomOutBtn")
    public void pressButtonZoomOutBtn() { staysPage.pressBtn(ZOOM_OUT_MAP_BTN); }

    @And("verify if searchThisAreaBtn exists")
    public void verifyIfSearchThisAreaBtnExists() { staysPage.isDisplayed(SEARCH_THIS_AREA_BTN); }

    @And("press button searchThisAreaBtn")
    public void pressButtonSearchThisAreaBtn() { staysPage.pressBtn(SEARCH_THIS_AREA_BTN); }

    @And("click on first element from the mapItemsList")
    public void clickOnFirstElementFromTheMapItemsList() { staysPage.getFirstElementFromTheList(MAP_PIN_ITEM); }

    @Then("verify if mapPreviewThumbnail exists")
    public void verifyIfMapPreviewThumbnailExists() { staysPage.isDisplayed(MAP_PREVIEW_THUMBNAIL); }

    @Then("verify if moreFiltersBtn has text {string}")
    public void verifyIfMoreFiltersBtnHasText(String arg0) { $(MORE_FILTERS_BTN).shouldHave(Condition.text(arg0)); }

    @And("verify if button typeOfPlaceBtn not pressed")
    public void verifyIfButtonTypeOfPlaceBtnNotPressed() { staysPage.isBtnPressed(TYPE_OF_PLACE_BTN, false); }

    @And("enter email {string} and password {string}")
    public void enterEmailAndPassword(String arg0, String arg1) {
        mainPage.typeLoginEmail(arg0);
        mainPage.typeLoginPassword(arg1);
    }

    @And("press button addAPlaceToTheMapBtn")
    public void pressButtonAddAPlaceToTheMapBtn() { staysPage.pressBtn(ADD_A_PLACE_TO_THE_MAP_BTN); }

    @And("verify if findAPlaceInput exists")
    public void verifyIfFindAPlaceInputExists() { staysPage.isDisplayed(FIND_A_PLACE_INPUT); }

    @And("type text {string} into findAPlaceInput")
    public void typeTextIntoFindAPlaceInput(String arg0) { staysPage.typeTextInto(FIND_A_PLACE_INPUT, arg0); }

    @And("verify if findAPlaceSuggestionPanel exists")
    public void verifyIfFindAPlaceSuggestionPanelExists() { staysPage.isDisplayed(FIND_A_PLACE_SEARCH_SUGGESTIONS_PANEL); }

    @And("select first suggestion from findAPlaceSuggestionPanel")
    public void selectFirstSuggestionFromFindAPlaceSuggestionPanel() { staysPage.pressBtn(FIRST_FIND_A_PLACE_SUGGESTION); }
}
