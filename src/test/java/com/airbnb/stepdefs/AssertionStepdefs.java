package com.airbnb.stepdefs;

import com.airbnb.pageObjects.StaysPage;
import com.airbnb.runners.World;
import io.cucumber.java.en.And;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import static com.airbnb.pageObjects.StaysPage.STAYS_ITEM_LIST;

public class AssertionStepdefs {

    public static WebDriver driver;
    StaysPage staysPage = new StaysPage(driver);

    private static String host1_name;
    private static String host2_name;
    private final World world;

    public AssertionStepdefs(World world) {
        this.world = world;
    }

    @And("get first name from the hosts list")
    public String getFirstNameFromTheHostsList() {
        String name1 = staysPage.getFirstHostNameFromTheList(STAYS_ITEM_LIST);
        System.out.println("host1: "+name1);
        world.setHost1_name(name1);
        return host1_name;
    }

    @And("get first name from the hosts list again")
    public String getFirstNameFromTheHostsListAgain() {
        String name2 = staysPage.getFirstHostNameFromTheList(STAYS_ITEM_LIST);
        System.out.println("host2: "+name2);
        world.setHost2_name(name2);
        return host2_name;
    }

    @And("verify if names are equal")
    public void verifyIfNamesAreEqual() {
        System.out.println(world.getHost1_name()+" and "+world.getHost2_name());
        Assert.assertEquals(world.getHost1_name(), world.getHost2_name());
    }

    @And("verify if names are not equal")
    public void verifyIfNamesAreNotEqual() {
        System.out.println(world.getHost1_name()+" and "+world.getHost2_name());
        Assert.assertNotEquals(world.getHost1_name(), world.getHost2_name());
    }
}
