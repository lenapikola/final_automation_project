package com.airbnb.runners;

public class World {

    private String host1_name = "";
    private String host2_name = "";

    public String getHost1_name() {
        return host1_name;
    }

    public void setHost1_name(String host1_name) {
        this.host1_name = host1_name;
    }

    public String getHost2_name() {
        return host2_name;
    }

    public void setHost2_name(String host2_name) {
        this.host2_name = host2_name;
    }

}
