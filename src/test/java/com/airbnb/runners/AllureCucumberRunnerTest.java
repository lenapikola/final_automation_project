package com.airbnb.runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(glue = {"com.airbnb.runners", "com.airbnb.stepdefs"},
        features = { "classpath:features" },
        plugin = { "pretty", "json:target/cucumber/cucumber.json" })
public class AllureCucumberRunnerTest {
}
