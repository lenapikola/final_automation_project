package com.airbnb.pageObjects;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class StaysPage extends BasePage {

    public static final By TITLE = By.cssSelector("h1._14i3z6h");
    public static final By TRAVEL_TITLE = By.xpath("//*[text()='Travel the world with Airbnb']");
    public static final By STAYS_ITEM_LIST = By.xpath("//a[@class='_gjfol0']");
    public static final By MORE_FILTERS_BTN = By.xpath("//div[@data-testid='menuItemButton-dynamicMoreFilters']");
    public static final By BEDS_INCREASE_BTN =
            By.xpath("//button[@data-testid='filterItem-rooms_and_beds-stepper-min_beds-0-increase-button']");
    public static final By BEDS_QUANTITY_VALUE =
            By.xpath("//span[@data-testid='filterItem-rooms_and_beds-stepper-min_beds-0-value']");
    public static final By BATHROOMS_INCREASE_BTN =
            By.xpath("//button[@data-testid='filterItem-rooms_and_beds-stepper-min_bathrooms-0-increase-button']");
    public static final By BATHROOMS_QUANTITY_VALUE =
            By.xpath("//span[@data-testid='filterItem-rooms_and_beds-stepper-min_bathrooms-0-value']");
    public static final String GYM_CHECKBOX = "#filterItem-facilities-checkbox-amenities-15";
    public static final String AIR_CONDITIONING_CHECKBOX = "#filterItem-amenities-checkbox-amenities-5";
    public static final String POOL_CHECKBOX = "#filterItem-facilities-checkbox-amenities-7";
    public static final String PETS_ALLOWED_CHECKBOX = "#filterItem-house_rules-checkbox-amenities-12";
    public static final String ENGLISH_CHECKBOX = "#filterItem-host_language-checkbox-languages-1";
    public static final By SUPERHOST_BTN = By.xpath("//button[@id='filterItem-other_options-switch-superhost-true']");
    public static final By CLEAR_ALL_FILTERS_BTN =
            By.xpath("//button[contains(@class, '_ejra3kg')] [contains(text(), 'Clear all')]");
    public static final By MORE_FILTERS_SUBMIT_BTN =
            By.xpath("//button[@data-testid='more-filters-modal-submit-button']");
    public static final By SHOW_MORE_AMENITIES_BTN =
            By.xpath("//span[contains(@class, '_jro6t0')] [contains(text(), 'Show all amenities')]");
    public static final By CLOSE_AMENITIES_BTN =
            By.xpath("//span[contains(@class, '_jro6t0')] [contains(text(), 'Close amenities')]");
    public static final String IRON_CHECKBOX = "#filterItem-amenities-checkbox-amenities-46";
    public static final By TYPE_OF_PLACE_BTN = By.xpath("//div[@data-testid='menuItemButton-room_type']//button");
    public static final String PRIVATE_ROOM_CHECKBOX = "#filterItem-room_type-checkbox-room_types-Private_room";
    public static final String HOTEL_ROOM_CHECKBOX = "#filterItem-room_type-checkbox-room_types-Hotel_room";
    public static final By FILTER_PANEL_CLEAR_BTN = By.xpath("//button[@data-testid='filter-panel-clear-button']");
    public static final By FILTER_PANEL_SAVE_BTN = By.xpath("//button[@data-testid='filter-panel-save-button']");
    public static final By TYPE_OF_PLACE_MENU_PANEL = By.xpath("//div[@data-testid='menuBarPanel-room_type']");
    public static final By PRICE_BTN = By.xpath("//div[@data-testid='menuItemButton-price_range']//button");
    public static final By MIN_PRICE_INPUT = By.xpath("//input[@id='price_filter_min']");
    public static final By MAX_PRICE_INPUT = By.xpath("//input[@id='price_filter_max']");
    public static final By MAP_PANEL = By.xpath("//div[@data-veloute='map/GoogleMap']");
    public static final By CLOSE_MAP_BTN = By.xpath("//button[@aria-label='Hide map']");
    public static final By ADD_A_PLACE_TO_THE_MAP_BTN = By.xpath("//button[@aria-label='Add a place to the map']");
    public static final By FIND_A_PLACE_INPUT = By.xpath("//label[@for='Koan-map-poi-search-koan__input']");
    public static final By FIND_A_PLACE_SEARCH_SUGGESTIONS_PANEL = By.xpath("//*[@aria-label='Search suggestions]");
    public static final By FIRST_FIND_A_PLACE_SUGGESTION = By.xpath("//li[@id=#Koan-map-poi-search-koan__option-0");
    public static final By SHOW_MAP_BTN = By.className("_fly27wr");
    public static final String SEARCH_AS_I_MOVE_THE_MAP_CHECKBOX = "#home-search-map-refresh-control-checkbox";
    public static final By ZOOM_IN_MAP_BTN = By.xpath("//button[@data-testid='map/ZoomInButton']");
    public static final By ZOOM_OUT_MAP_BTN = By.xpath("//button[@data-testid='map/ZoomOutButton']");
    public static final By SEARCH_THIS_AREA_BTN = By.xpath("//button[@data-testid='map-manual-refresh-button']");
    public static final By MAP_PIN_ITEM = By.className("_1nq36y92");
    public static final By MAP_PREVIEW_THUMBNAIL = By.className("_i24ijs");
    public static final By ITEM_PREVIEW_IMAGE = By.className("_15tommw");
    public static final By NEXT_IMAGE_BTN = By.xpath("//button[@aria-label='Next image']");
    public static final By PREVIOUS_IMAGE_BTN = By.xpath("//button[@aria-label='Previous image']");
    public static final By PAGINATION_PANEL = By.xpath("//div[@aria-label='Search results pagination']");


    private final WebDriver driver;

    public StaysPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public boolean isTitleDisplayed() {
        return $(TITLE).isDisplayed();
    }

    public boolean isTravelTitleDisplayed() {
        return $(TRAVEL_TITLE).isDisplayed();
    }

    public void isTitleHasText(String textStr) {
        $(TITLE).shouldHave(Condition.text(textStr));
    }

    public void isTravelTitleHasText(String textStr) {
        $(TRAVEL_TITLE).shouldHave(Condition.text(textStr));
    }

    public void isLittleLocationSearchBtnHasText(String textStr) {
        $$(LITTLE_SEARCH_BUTTON).get(0).shouldHave(Condition.text(textStr));
    }

    public void isLittleGuestsSearchBtnHasValue(String value) {
        $$(LITTLE_SEARCH_BUTTON).get(2).shouldHave(Condition.text(value));
    }

    public void checkCheckbox(String element) {
        $(element).click();
    }

    public void isCheckboxChecked(String element) {
        $(element).shouldHave(Condition.attribute("class", "_ravnr26"));
    }

    public void isCheckboxUnchecked(String element) {
        $(element).shouldHave(Condition.attribute("class", "_19t2uhr"));
    }

    public void isCheckboxNotVisible(String element) {
        $(element).shouldNotBe(Condition.visible);
    }

    public void isBtnPressed(By element, Boolean value) {
        $(element).shouldHave(Condition.attribute("aria-pressed", String.valueOf(value)));
    }

    public void isBtnChecked(By element, Boolean value) {
        $(element).shouldHave(Condition.attribute("aria-checked", String.valueOf(value)));
    }

    public void doubleClickOnElement(By element) { $(element).doubleClick(); }

    public void typeTextInto(By element, String value) { $(element).sendKeys(value); }

    public void tabToNextElementFrom(By element) { $(element).pressTab(); }

    public void isElementFocused(By element) { $(element).shouldBe(Condition.focused); }

    public void getFirstElementFromTheList(By element) { $$(element).get(1).click(); }

    public String getFirstHostNameFromTheList(By element) { return $$(element).get(1).getAttribute("aria-label");
    }

//    public void showNextImageOnMapPreviewThumbnail(By element) {
//        $$(element).last().click();
//    }
//
//    public void showPreviousImageOnMapPreviewThumbnail(By element) {
//        $$(element).get(-2).click();
//    }

}
