package com.airbnb.pageObjects;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.File;
import java.util.ArrayList;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class BasePage {

    @FindBy(xpath = "//input[@data-testid='structured-search-input-field-query']")
    public WebElement locationInput;

    @FindBy(xpath = "//button[@data-testid='structured-search-input-search-button']")
    public WebElement searchBtn;



    public static final By LITTLE_SEARCH_PANEL = By.xpath("//div[@data-testid='little-search']");
    public static final By LITTLE_SEARCH_BUTTON = By.xpath("//button[@class='_b2fxuo']");
    public static final By FOOTER = By.xpath("//footer[@role='contentinfo']");
    public static final By PROFILE_BTN = By.xpath("//button[@data-testid='cypress-headernav-profile']");
    public static final By PROFILE_MENU = By.xpath("//div[@data-testid='simple-header-profile-menu']");
    public static final By LOGIN_BTN = By.xpath("//a[@data-testid='cypress-headernav-login']");
    public static final By POPUP_MODAL = By.xpath("//div[@role='dialog']");
    public static final By LOGIN_WITH_EMAIL_BTN = By.xpath("//button[@data-testid='social-auth-button-email']");
    public static final By EMAIL_LOGIN_INPUT = By.xpath("//input[@data-testid='login-signup-email']");
    public static final By PASSWORD_LOGIN_INPUT = By.xpath("//input[@data-testid='login-signup-password']");
    public static final By SUBMIT_LOGIN_BTN = By.xpath("//button[@data-testid='signup-login-submit-btn']");
    public static final By LANGUAGE_CURRENCY_BTN = By.xpath("//button[@aria-label='Language and currency']");
    public static final By SPANISH_LANGUAGE_BTNS = By.xpath("//*[contains(text(),'Español')]");
    public static final By CURRENCY_BTN = By.xpath("//span[@class='_1ezr7jp']");
    public static final By CURRENCIES_LIST = By.xpath("//button[@class='_lwi6c1u']");
    public static final By SELECTED_LANG_CURR_ICON = By.xpath("//span[@class='_14tkmhr']");
    public static final By SELECTED_LANG_CURR_NAME = By.xpath("//span[@class='_bjxj6b']");

    private final WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isDisplayed(By element) { return $(element).isDisplayed(); }

    public void isNotDisplayed(By element) { $(element).shouldNotBe(Condition.exist); }

    public void scrollToView(By element) { $(element).scrollIntoView(true); }

    public void pressBtn(By element) { $(element).click(); }

    public void selectSpanishFromLanguages() { $$(SPANISH_LANGUAGE_BTNS).get(4).click(); }

    public void isFooterLanguageNameEqualsText(String textStr)  {
        $$(SELECTED_LANG_CURR_NAME).get(0).shouldHave(Condition.text(textStr));
    }

    public void selectHRKFromCurrencies() { $$(CURRENCIES_LIST).get(8).click(); }

    public void isFooterCurrencyIconEqualsValue(String value) {
        $$(SELECTED_LANG_CURR_ICON).get(1).shouldHave(Condition.text(value));
    }

    public void isFooterCurrencyNameEqualsText(String value) {
        $$(SELECTED_LANG_CURR_NAME).get(1).shouldHave(Condition.text(value));
    }

    public void typeLoginEmail(String email) { $(EMAIL_LOGIN_INPUT).sendKeys(email); }

    public void typeLoginPassword(String password) { $(PASSWORD_LOGIN_INPUT).sendKeys(password); }

    public void changeQuantity(By element) {
        $(element).click();
    }

    public void isElementHasType(String element, String type) {
        $(element).shouldHave(Condition.type(type));
    }

    public void isElementHasValue(By element, String value) {
        $(element).shouldHave(Condition.value(value));
    }

    public void isElementHasText(By element, String text) {
        $(element).shouldHave(Condition.text(text));
    }

    public void isBtnEnabled(By element) {
        $(element).shouldBe(Condition.enabled);
    }

    public void isBtnDisabled(By element) {
        $(element).shouldNotBe(Condition.enabled);
    }

    public void switchToBrowserTab(int tab) {
        ArrayList<String> tabs = new ArrayList<> (getWebDriver().getWindowHandles());
        getWebDriver().switchTo().window(tabs.get(tab));
    }

    public void isCurrentLinkContainsText(String text) {
        assert getWebDriver().getCurrentUrl().contains(text);
    }

    public void waitUntilElementIsVisible(By element) { $(element).waitUntil(Condition.exist, 10000); }

    public File getScreenshotOfElement(By element) { return $(element).screenshot(); }

    public void hoverOverElement(By element) { $(element).hover(); }

}