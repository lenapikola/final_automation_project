package com.airbnb.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HostPage extends BasePage {

    private final WebDriver driver;

    public HostPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public static By HOST_TITLE = By.xpath("//span[@class='_10pmr93']");
    public static By SHOW_ALL_AMENITIES_BTN = By.xpath("//a[contains (@class, '_13e0raay')] [contains(text(), 'amenities')]");
    public static By CLOSE_AMENITIES_POPUP_BTN = By.xpath("//button[@aria-label='Close']");
    public static By POLICIES_SECTIONS = By.xpath("//div[@class='_m9x7bnz']");
    public static By HOST_CONTACT_INFO = By.xpath("//*[contains(@class, '_1q2lt74')] [contains(text(), 'Languages')]");
    public static By BOOKING_INFO_SECTION = By.xpath("//div[@data-testid='book-it-default']");
    public static String TITLE_PHOTO = "#FMP-target";


}
