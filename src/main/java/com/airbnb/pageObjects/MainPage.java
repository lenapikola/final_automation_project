package com.airbnb.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.codeborne.selenide.Selenide.$;

public class MainPage extends BasePage {

    public static final By LOCATION_INPUT = By.xpath("//input[@data-testid='structured-search-input-field-query']");
    public static final By SEARCH_BTN = By.xpath("//button[@data-testid='structured-search-input-search-button']");
    public static final By EXPLORE_NEARBY_BTN = By.xpath("//li[@id='Koan-query__option-0']");
    public static final By DATES_INPUT = By.xpath("//div[@data-testid='structured-search-input-field-split-dates-0']");
    public static final By DATES_PANEL = By.cssSelector("#structured-search-input-field-dates-panel");
    public static final By CHECK_IN_DATE = By.xpath("//div[@data-testid='datepicker-day-2021-01-28']");
    public static final By CHECK_OUT_DATE = By.xpath("//div[@data-testid='datepicker-day-2021-02-06']");
    public static final By GUESTS_BTN = By.xpath("//div[@data-testid='structured-search-input-field-guests-button']");
    public static final By GUESTS_PANEL = By.xpath("//div[@data-testid='structured-search-input-field-guests-panel']");
    public static final By ADULTS_DECREASE_BTN = By.xpath("//button[@data-testid='stepper-adults-decrease-button']");
    public static final By ADULTS_INCREASE_BTN = By.xpath("//button[@data-testid='stepper-adults-increase-button']");
    public static final By ADULTS_QUANTITY_VALUE = By.xpath("//span[@data-testid='stepper-adults-value']");
    public static final By CHILDREN_DECREASE_BTN = By.xpath("//button[@data-testid='stepper-children-decrease-button']");
    public static final By CHILDREN_INCREASE_BTN = By.xpath("//button[@data-testid='stepper-children-increase-button']");
    public static final By CHILDREN_QUANTITY_VALUE = By.xpath("//span[@data-testid='stepper-children-value']");
    public static final By INFANTS_DECREASE_BTN = By.xpath("//button[@data-testid='stepper-infants-decrease-button']");
    public static final By INFANTS_INCREASE_BTN = By.xpath("//button[@data-testid='stepper-infants-increase-button']");
    public static final By INFANTS_QUANTITY_VALUE = By.xpath("//span[@data-testid='stepper-infants-value']");

    private final WebDriver driver;

    public MainPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public void typeLocation(String searchStr) {
        $(LOCATION_INPUT).sendKeys(searchStr);
    }

    public void selectCheckInDate() { $(CHECK_IN_DATE).click(); }

    public void selectCheckOutDate() { $(CHECK_OUT_DATE).click(); }


}
